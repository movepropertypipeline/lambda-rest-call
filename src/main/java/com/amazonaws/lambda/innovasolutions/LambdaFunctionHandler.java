package com.amazonaws.lambda.innovasolutions;

import java.util.List;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.event.S3EventNotification.S3EventNotificationRecord;


public class LambdaFunctionHandler implements RequestHandler<S3Event, String> {

	public LambdaFunctionHandler() {}
	@Override
	public String handleRequest(S3Event event, Context context)
	{

		context.getLogger().log("Received event: " + event);

		List<S3EventNotificationRecord> records = event.getRecords();


		JSONObject js = new JSONObject();
		for (S3EventNotificationRecord record : records)
		{
			// following attributes belong to Event object in rest service which is deployed on ec2 this json ll be mapped to that object 
			js.put("awsRegion", record.getAwsRegion());
			js.put("bucketName", record.getS3().getBucket().getName());
			js.put("eventName", record.getEventName());
			js.put("eventMessage", "DISPATCH");
			js.put("configurationId", record.getS3().getConfigurationId());
			js.put("fileName", record.getS3().getObject().getKey());

		}



		String uri = new String("http://ec2-18-188-144-127.us-east-2.compute.amazonaws.com:8080/update");

		try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) 
		{
			
			HttpPost putRequest = new HttpPost(uri);

			StringEntity input = new StringEntity(js.toString());
			input.setContentType("application/json");
			putRequest.setEntity(input);

			httpClient.execute(putRequest);
			
			return "Successfully inserted records in dynamodb.";
		} 
		catch (Exception e)
		{
			e.printStackTrace();

		}

		return "Failed to make rest call to EC2";
	}
}